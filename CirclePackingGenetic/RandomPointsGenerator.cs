// <copyright file="RandomPointsGenerator.cs" company="Vladislav Antonyuk">
//     Vladislav Antonyuk. All rights reserved.
// </copyright>
// <author>Vladislav Antonyuk</author>

namespace CirclePacking
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using CirclePackingGenetic;

    public class RandomPointGenerator
    {
        private readonly Random _randy = new Random();

        public List<MyPoint> GetPointsInACircle(int radius, int numberOfPoints)
        {
            var points = new List<MyPoint>();
            for (var pointIndex = 0; pointIndex < numberOfPoints; pointIndex++)
            {
                var distance = _randy.Next(radius);
                var angleInRadians = _randy.Next(360) / (2 * Math.PI);

                var x = (int)(distance * Math.Cos(angleInRadians));
                var y = (int)(distance * Math.Sin(angleInRadians));
                var randomPoint = new MyPoint(x, y);
                points.Add(randomPoint);
            }

            return points;
        }

        public List<Point> MovePoints(List<Point> oldPoints, double eps)
        {
            var points = new List<Point>();
            foreach (var p in oldPoints)
            {
                var x = (_randy.Next(-100, 100) < 0 ? -1 : 1) * _randy.NextDouble() * eps;
                var y = (_randy.Next(-100, 100) < 0 ? -1 : 1) * _randy.NextDouble() * eps;
                points.Add(new Point(p.X + x, p.Y + y));
            }

            return points;
        }

        public Pair Mutation(Pair pair, double eps)
        {
            /*foreach (var item in pair.Points)
            {
                var x = (_randy.Next(-100, 100) < 0 ? -1 : 1) * _randy.NextDouble() * eps;
                var y = (_randy.Next(-100, 100) < 0 ? -1 : 1) * _randy.NextDouble() * eps;
                item.X = item.X + x;
                item.Y = item.Y + y;
            }*/

var i = new Random().Next(0,pair.Points.Count()-1);
var x = (_randy.Next(-100, 100) < 0 ? -1 : 1) * _randy.NextDouble() * eps;
                var y = (_randy.Next(-100, 100) < 0 ? -1 : 1) * _randy.NextDouble() * eps;
                 pair.Points[i].X += x;
                pair.Points[i].Y += y;


            return pair;
        }
    }
}