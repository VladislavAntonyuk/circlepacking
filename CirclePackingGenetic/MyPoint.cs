using System.Collections.Generic;
using System;
using System.Linq;
namespace CirclePacking
{
    public class MyPoint
    {
        public MyPoint(double x,double y)
        {
            X=x;
            Y=y;
        }

        public double X { get; set; }
        public double Y { get; set; }
        public double RToCircle { get; set; }
        public Dictionary<int, double> RToPoints { get; set; } = new Dictionary<int, double>();        
    }
}