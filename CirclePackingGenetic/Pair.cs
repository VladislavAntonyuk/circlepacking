using System;
using System.Collections.Generic;
using System.Linq;
using CirclePacking;

namespace CirclePackingGenetic
{
    public class Pair
    {
        public List<MyPoint> Points { get; set; } = new List<MyPoint>();
        public double Fitness
        {
            get
            {
                double f = double.MaxValue;
                foreach (var p in Points)
                {
                    var pf = Math.Min(p.RToCircle, p.RToPoints.Select(v => v.Value).Min());
                    if (pf < f)
                        f = pf;
                }
                if (f == double.MaxValue) f = 0;
                return f;
            }
        }

        public override string ToString()
        {
            return $"{Fitness}";
        }
    }
}