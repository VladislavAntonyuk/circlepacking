// <copyright file="RandomPointsGenerator.cs" company="Vladislav Antonyuk">
//     Vladislav Antonyuk. All rights reserved.
// </copyright>
// <author>Vladislav Antonyuk</author>

namespace CirclePacking
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;

    public class RandomPointGenerator
    {
        private readonly Random _randy = new Random();

        public List<Point> GetPointsInACircle(int radius, int numberOfPoints)
        {
            var points = new List<Point>();
            for (var pointIndex = 0; pointIndex < numberOfPoints; pointIndex++)
            {
                var distance = _randy.Next(radius);
                var angleInRadians = _randy.Next(360) / (2 * Math.PI);

                var x = (int) (distance * Math.Cos(angleInRadians));
                var y = (int) (distance * Math.Sin(angleInRadians));
                var randomPoint = new Point(x, y);
                points.Add(randomPoint);
            }

            return points;
        }

        public List<Point> MovePoints(List<Point> oldPoints, double eps)
        {
            var points = new List<Point>();
            foreach (var p in oldPoints)
            {
                var x = (_randy.Next(-100, 100) < 0 ? -1 : 1) * _randy.NextDouble() * eps;
                var y = (_randy.Next(-100, 100) < 0 ? -1 : 1) * _randy.NextDouble() * eps;
                points.Add(new Point(p.X + x, p.Y + y));
            }

            return points;
        }

        public List<Point> MovePoints2(List<Point> oldPoints, double eps)
        {
            var points = oldPoints;
            var i = _randy.Next(0, points.Count);
            var x = (_randy.Next(-100, 100) < 0 ? -1 : 1) * _randy.NextDouble() * eps;
            var y = (_randy.Next(-100, 100) < 0 ? -1 : 1) * _randy.NextDouble() * eps;
            points[i] = new Point(points[i].X + x, points[i].Y + y);

            return points;
        }        
    }
}