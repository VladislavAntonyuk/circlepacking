﻿// <copyright file="ICirclePacker.cs" company="Vladislav Antonyuk">
//     Vladislav Antonyuk. All rights reserved.
// </copyright>
// <author>Vladislav Antonyuk</author>

namespace CirclePacking
{
    internal interface ICirclePacker
    {
        double Iterate();
        void Render();
    }
}