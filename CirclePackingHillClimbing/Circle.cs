﻿// <copyright file="Circle.cs" company="Vladislav Antonyuk">
//     Vladislav Antonyuk. All rights reserved.
// </copyright>
// <author>Vladislav Antonyuk</author>

namespace CirclePacking
{
    using System;
    using System.Windows;
    using System.Windows.Media;

    public class Circle
    {
        public Color circleColor;
        public string CircleName;
        public double Height = 100.0f;
        public Vector mCenter;
        public double mRadius;
        public double Width = 100.0f;

        public Circle(Vector nCenter, double nRadius)
        {
            mCenter = nCenter;
            mRadius = nRadius;
            circleColor = Color.FromArgb(255, 156, 156, 156);
        }

        public double DistanceToCenter
        {
            get
            {
                var dx = mCenter.X - Width / 2;
                var dy = mCenter.Y - Height / 2;
                return Math.Sqrt(dx * dx + dy * dy);
            }
        }

        public bool Contains(double x, double y)
        {
            var dx = mCenter.X - x;
            var dy = mCenter.Y - y;
            return Math.Sqrt(dx * dx + dy * dy) <= mRadius;
        }

        public bool Intersects(Circle c)
        {
            var dx = c.mCenter.X - mCenter.X;
            var dy = c.mCenter.Y - mCenter.Y;
            var d = Math.Sqrt(dx * dx + dy * dy);
            return d < mRadius || d < c.mRadius;
        }
    }
}