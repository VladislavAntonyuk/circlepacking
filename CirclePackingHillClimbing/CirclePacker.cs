﻿// <copyright file="CirclePacker.cs" company="Vladislav Antonyuk">
//     Vladislav Antonyuk. All rights reserved.
// </copyright>
// <author>Vladislav Antonyuk</author>

namespace CirclePacking
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Shapes;

    public class CirclePacker : ICirclePacker
    {
        public readonly double eps = 0.0001;

        private readonly double radius;
        
        /// <summary>
        /// </summary>
        private bool min;

        /// <summary>
        ///     Generates a number of Packing circles in the constructor.
        ///     Random distribution is linear
        /// </summary>
        public CirclePacker(Canvas hostCanvas, double radius, int pNumCircles)
        {
            HostCanvas = hostCanvas;
            this.radius = radius;
            AllCircles = new List<Circle>();
            // Create random circles
            AllCircles.Clear();
            var Rnd = new Random(DateTime.Now.Millisecond);
            for (var i = 0; i < pNumCircles; i++)
            {
            var nCenter = new Vector(radius + Rnd.NextDouble() * eps, radius + Rnd.NextDouble() * eps);
                AllCircles.Add(new Circle(nCenter, eps));
            }
        }

        public List<Circle> AllCircles { get; set; }
        public Canvas HostCanvas { get; set; }

        public double Iterate()
        {
            var curRad = eps;
            while (!min)
            {
                foreach (var t in AllCircles)
                {
                    t.mRadius += eps;
                    curRad = t.mRadius;
                }

                // Sort circles based on the distance to center
                var sortedCircles = from c in AllCircles
                    orderby c.DistanceToCenter descending
                    select c;

                AllCircles = sortedCircles.ToList();

                var minSeparationSq = eps * eps;
                for (var i = 0; i < AllCircles.Count - 1; i++)
                {
                    for (var j = i + 1; j < AllCircles.Count; j++)
                    {
                        if (i == j)
                        {
                            continue;
                        }

                        var AB = AllCircles[j].mCenter - AllCircles[i].mCenter;
                        var r = AllCircles[i].mRadius + AllCircles[j].mRadius;

                        // Length squared = (dx * dx) + (dy * dy);
                        var d = AB.LengthSquared - minSeparationSq;
                        var minSepSq = Math.Min(d, minSeparationSq);
                        d -= minSepSq;

                        if (d < r * r - eps)
                        {
                            AB.Normalize();

                            AB *= (r - Math.Sqrt(d)) * 0.5;

                            AllCircles[j].mCenter += AB;
                            AllCircles[i].mCenter -= AB;
                        }
                    }
                }

                var d1 = AllCircles.Select(t => new Vector(t.mCenter.X, t.mCenter.Y)).Select(v1 => radius - Math.Sqrt(Math.Pow(v1.X - radius, 2) + Math.Pow(v1.Y - radius, 2))).ToList();

                d1.Sort();

                foreach (var t in AllCircles)
                {
                    if (d1[0] < eps || d1[0] - t.mRadius < eps)
                    {
                        min = true;
                    }
                }

                if (curRad > radius / 2)
                {
                    min = true;
                }
            }

            return AllCircles[0].mRadius;
        }

        public void Render()
        {
            HostCanvas.Children.Clear();
            for (var i = 0; i < AllCircles.Count; i++)
            {
                var c = AllCircles[i];
                // Just in case there are some NaN values out there
                if (!c.mCenter.X.ToString().Contains("∞") && !c.mCenter.Y.ToString().Contains("∞"))
                {
                    if (i < HostCanvas.Children.Count)
                    {
                        var e = (Ellipse) HostCanvas.Children[i];
                        e.Width = e.Height = c.mRadius * 2;
                        e.Fill = new SolidColorBrush(c.circleColor);
                        e.Stroke = Brushes.Black;
                        Canvas.SetLeft(e, c.mCenter.X - c.mRadius);
                        Canvas.SetTop(e, c.mCenter.Y - c.mRadius);
                    }
                    else
                    {
                        var e = new Ellipse();
                        e.Width = e.Height = c.mRadius * 2;
                        e.Fill = new SolidColorBrush(c.circleColor);
                        e.Stroke = Brushes.Black;
                        Canvas.SetLeft(e, c.mCenter.X - c.mRadius);
                        Canvas.SetTop(e, c.mCenter.Y - c.mRadius);
                        HostCanvas.Children.Add(e);
                    }
                }
            }

            var mainCircle = new Ellipse
            {
                Width = 100 * 2,
                Height = 100 * 2,
                Stroke = Brushes.Black,
                StrokeThickness = 1
            };
            HostCanvas.Children.Add(mainCircle);
        }
    }
}