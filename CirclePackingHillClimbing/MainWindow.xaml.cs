﻿// <copyright file="MainWindow.xaml.cs" company="Vladislav Antonyuk">
//     Vladislav Antonyuk. All rights reserved.
// </copyright>
// <author>Vladislav Antonyuk</author>

namespace CirclePacking
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.DataVisualization.Charting;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Shapes;

    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            CircleCanvas.Width = CircleCanvas.Height;
            CircleCanvas2.Width = CircleCanvas2.Height;
            // mcChart.Width = mcChart.Height;
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void CircleCount_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (CircleCount.Text.Length == 0)
            {
                return;
            }

            var cCount = Convert.ToInt32(CircleCount.Text);
            if (cCount < 2)
            {
                return;
            }

            var iter = 5000;

            var radius = Convert.ToInt32(CircleCanvas.Width / 2);
            var radiuses = new List<double>();

            CircleCanvas.Children.Clear();
            var cp = new CirclePacker(CircleCanvas2, radius, Convert.ToInt32(CircleCount.Text));
            var eps = cp.eps;
            radiuses.Add(cp.Iterate());
            cp.Render();
            Title = radiuses[0].ToString();
            //RCanvas.Children.Clear();
            //center -radius - radius
            var mainCircle = new Ellipse
            {
                Width = radius * 2,
                Height = radius * 2,
                Stroke = Brushes.Black,
                StrokeThickness = 1
            };
            CircleCanvas.Children.Add(mainCircle);
            var pointGen = new RandomPointGenerator();
            // var randomPoints2 = pointGen.GetPointsInACircle(radius, cCount);
            var randomPoints2 = cp.AllCircles.Select(t => new Point(t.mCenter.X - radius, t.mCenter.Y - radius)).ToList();

            foreach (var point in randomPoints2)
            {
                var mainCircle2 = new Ellipse
                {
                    Width = 5,
                    Height = 5,
                    Stroke = Brushes.Red,
                    StrokeThickness = 1
                };
                Canvas.SetLeft(mainCircle2, point.X + radius);
                Canvas.SetTop(mainCircle2, point.Y + radius);
                CircleCanvas.Children.Add(mainCircle2);
                //       g.DrawEllipse(p, point.X + radius, point.Y+radius, 2, 2);
            }

            double prevMinD = radius;
            double prevMinB = radius * 2;
            double prevMin = radiuses[0];
            var iterCount = 0;
            double min = 0;
            for (var a = 0; a < iter; a++)
            {
                foreach (var point in randomPoints2)
                {
                    var mainCircle2 = new Ellipse
                    {
                        Width = 5,
                        Height = 5,
                        Stroke = Brushes.Red,
                        StrokeThickness = 1
                    };
                    Canvas.SetLeft(mainCircle2, point.X + radius);
                    Canvas.SetTop(mainCircle2, point.Y + radius);
                    CircleCanvas.Children.Add(mainCircle2);
                    //       g.DrawEllipse(p, point.X + radius, point.Y+radius, 2, 2);
                }

                var randomPoints = pointGen.MovePoints2(randomPoints2, eps);
                var d1 = new List<double>();
                for (var i = 0; i < randomPoints.Count; i++)
                {
                    d1.Add(radius - Math.Sqrt(Math.Pow(randomPoints[i].X, 2) + Math.Pow(randomPoints[i].Y, 2)));
                }

                var minD = d1.Min();
                if (minD < 0 || prevMinD < minD)
                {
                    continue;
                }

                var dBetweenCircles = new List<double>();
                for (var i = 0; i < randomPoints.Count - 1; i++)
                {
                    for (var j = i + 1; j < randomPoints.Count; j++)
                    {
                        if (i == j)
                        {
                            continue;
                        }

                        var d = 0.5 * Math.Sqrt(Math.Pow(randomPoints[i].X - randomPoints[j].X, 2) +
                                                Math.Pow(randomPoints[i].Y - randomPoints[j].Y, 2));
                        dBetweenCircles.Add(d);
                    }
                }

                var minB = dBetweenCircles.Min();
                if (prevMinB < minB)
                {
                    continue;
                }

                min = Math.Min(minB, minD);
                if (prevMin < min)
                {
                    prevMin = min;
                    randomPoints2 = randomPoints;
                    radiuses.Add(min);
                    iterCount++;
                }
                else
                {
                    eps /= 2;
                    if (eps < 0.000001)
                    {
                        eps = 0.000001;
                    }
                }
            }

            var maxR = radiuses.Max();
            Title += " " + maxR + " " + iterCount + " " + eps;

            var ri = 0;
            ((LineSeries) mcChart.Series[0]).ItemsSource = radiuses.Select(r => new KeyValuePair<int, double>(ri++, r));

            foreach (var point in randomPoints2)
            {
                var mainCircle3 = new Ellipse
                {
                    Width = 5,
                    Height = 5,
                    Stroke = Brushes.Black,
                    StrokeThickness = 1
                };

                Canvas.SetLeft(mainCircle3, point.X + radius);
                Canvas.SetTop(mainCircle3, point.Y + radius);
                CircleCanvas.Children.Add(mainCircle3);
                //Text(point.X + radius, point.Y + radius, point.X + radius + " " + point.Y + radius);
                //       g.DrawEllipse(p, point.X + radius, point.Y+radius, 2, 2);
            }
        }

        private void Text(double x, double y, string text)
        {
            var textBlock = new TextBlock();
            textBlock.Text = text;
            textBlock.Foreground = new SolidColorBrush(Color.FromRgb(150, 150, 150));
            Canvas.SetLeft(textBlock, x);
            Canvas.SetTop(textBlock, y);
            CircleCanvas.Children.Add(textBlock);
        }
    }
}